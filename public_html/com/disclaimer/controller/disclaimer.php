<?php
/**
 * Disclaimer controller
 *
 * Handles disclaimer agreements
 *
 * Jan 19, 2019
 *
 * @version     0.1
 * @abstract
 * @package     kernel
 * @author      Translucide
 * @copyright   copyright (c) 2012 Translucide
 * @license
 * @since       0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/template/template');
$service->get('Ressource')->get('com/disclaimer/lang/'.$service->get('Language')->getCode().'/disclaimer');

class DisclaimerController extends Controller{
    public function init(){
        $this->component = 'disclaimer';
    }

    public function execute(){
        global $service;
        $request = $service->get('Request')->get();
        $url = $service->get('Url')->get();
        $view = $this->getView();
        $authentified = $service->get('User')->isLogged();

        //User must be logged in to sign a disclaimer.
        if (!$authentified) {
            //Try to redirect user the best we can...
            $this->redirect();
        }
        $op = $request['op'];
        if ($op == '') $op = 'view';
        switch($op) {
            case 'sign' : {
                if ($request['disclaimer_agreed'] == 1) {
                    $this->createProfileFieldObj();

                    //Save agreement into user profile.
                    $profileStore = new ProfileStore();
                    $profileStore->setValue('disclaimer_lastagreed',time());
                    $obj = $profileStore->getKey('disclaimer_lastagreed');

                    //Trigger login success event again in case others are acting on it
                    //because we might have interfered with other modules on user login here.
                    $params = $service->get('EventHandler')->trigger('user.login.success',[
                        'redirect' => $request['redirect']
                    ]);

                    //Nobody redirect used at this point...
                    //Try to redirect the user as login controller would normally do.
    				$this->redirect($params['redirect']);
    				header('location: '.URL);
    				exit;
                }
            }
            case 'view':
            default:{
                $form = new Form(URL.$service->get('Language')->getCode().'/disclaimer/sign', 'disclaimer', '', 'POST');
                $form->option('layout','vertical');
                $form->add(new TextareaFormField('disclaimer_fulltext',$service->get('setting')->get('disclaimer_fulltext'),array(
                    'title'=>'',
                    'rows' => 20,
                )));
                $form->add(new CheckboxFormField('disclaimer_agreed',0,array(
                    'title'=>'',
                    'options' => [
                        ['title' => DISCLAIMER_CHECKBOX,'value'=> 1]
                    ]
                )));
        		$form->option('intro',DISCLAIMER_INTRO);
        		$form->add(new HiddenFormfield('op','sign'));
                $form->add(new SubmitFormField('submit','',array(
                    'title' => DISCLAIMER_SUBMIT,
                    'disabled' => 1
                )));
                $view->setVar('form',$form->render());
            }break;
        }
        return $view;
    }

    private function redirect($url = null) {
        global $service;
        $request = $service->get('Request')->get();
        if (is_null($url) && isset($request['redirect'])) $redirect = $service->get('Request')->get('redirect');
        else $redirect = $url;
        $redirect = str_replace('{url}',URL,$redirect);
        if (substr($redirect,0,1) == '/') $redirect = URL.$redirect;
        if ($redirect) {
            header('Location: '.$redirect);
            exit;
        }
    }

    /**
     * Returns the corresponding URL in the language ID given
     * or simply returns the home page url if none found
     *
     * @public
     * @param string $url The url for which we need the translation
     * @param int $langid The language ID to find the URL for
     * @return string The translated URL in the language we asked for or false if not found.
     */
    public function translate($langid, $objid = 0, $url=''){
        global $service;
        return $service->get('Language')->getCodeForId($langid).'/disclaimer';
    }

    /**
     * Checks if we need to create a profile field obj.
     * @var [type]
     */
    private function createProfilefieldObj() {
        global $service;
        $service->get('Ressource')->get('core/model/profilefield');

        //Check if we have ever created a profilefield before...
        $pfs = new ProfilefieldStore();
        $pf = $pfs->getByKey('disclaimer_lastagreed');
        if (!$pf || !is_object($pf)) {
            $obj = $pfs->create();
            $obj->setVar('profilefield_type','hidden');
            $obj->setVar('profilefield_key','disclaimer_lastagreed');
            $pfs->save($obj);
        }
    }
}
?>
