<?php
global $service;
$service->get('Ressource')->get('com/disclaimer/lang/'.$service->get('Language')->getCode().'/disclaimer.php');
$service->get('EventHandler')->on('user.login.success',
	function($e,$p){
		global $service;

        //Every time a user logs in, we intercept the login and redirect the user to our
        //disclaimer page....
        $service->get('Ressource')->get('core/model/profile');
        $profileStore = new ProfileStore();
        $val = $profileStore->getValue('disclaimer_lastagreed');

        $mode = $service->get('setting')->get('disclaimer_mode');
        $nbdays = intval($service->get('setting')->get('disclaimer_eachnbdays'));
        $date = $service->get('setting')->get('disclaimer_setdate');

        //Recurring mode
        if ($mode == 1) {
            if (!$val || $val + ($nbdays*24*60*60) < time()) {
                //We are due for a disclaimer sigining....
                @header('Location: '.URL.$service->get('Language')->getCode().'/disclaimer/view?redirect='.$service->get('Request')->get('redirect'));
                exit;
            }
        }
	},[
        'priority' => 25
    ]
);
?>
