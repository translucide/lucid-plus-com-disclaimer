<?php
define('DISCLAIMER_TITLE','Disclaimer');
define('DISCLAIMER_INTRO','You must agree on the current terms in order to user our website.');
define('DISCLAIMER_CHECKBOX','I have read and I agree on the terms above.');
define('DISCLAIMER_SUBMIT','Sign me in');
define('DISCLAIMER_ERROR','You must read the full disclaimer text before checking the \'I have read and I agree on the terms above\' checkbox.');
?>
