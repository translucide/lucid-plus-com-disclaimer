<?php
define('DISCLAIMER_SETTING_CATEGORY','Disclaimer');
define('DISCLAIMER_SETTING_FULLTEXT','Disclaimer text');
define('DISCLAIMER_SETTING_FULLTEXT_DESC','Enter the full disclaimer text the user needs to agree upon login to your website.');
define('DISCLAIMER_SETTING_MODE','Disclaimer mode');
define('DISCLAIMER_SETTING_MODE_DESC','Set to RECURRING mode to ask users every X days to agree on the disclaimer terms. Use AFTER SET DATE to force users into agreeing on disclaimer terms after a specific date (usually the date you will change the disclaimer text).');
define('DISCLAIMER_SETTING_EACHNBDAYS','Recurring mode : Nb days');
define('DISCLAIMER_SETTING_EACHNBDAYS_DESC','The max number of days before we ask the user to agree again on the disclaimer terms. Applicable only when selecting the RECURRING mode.');
define('DISCLAIMER_SETTING_SETDATE','Date of agreement');
define('DISCLAIMER_SETTING_EACHNBDAYS_DESC','The date after which all users must agree on the current disclaimer terms. Applies only when selecting the AFTER SET DATE mode.');
?>
