<?php
/**
 * Disclaimer view
 *
 * Jan 19, 2019
 *
 * @version     0.1
 * @abstract
 * @package     lucid-plus-com-emailcomposer
 * @author      Translucide
 * @copyright   copyright (c) 2012 Translucide
 * @license
 * @since       0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class DisclaimerView extends DefaultView{
    public function init(){
    }

    public function render(){
        global $service;
        $service->get('Ressource')->get('core/display/template');
        $request = $this->getVar('request');
        $form = $this->getVar('form');
        $tpl = (isset($request['op']))?substr($request['op'],0,25):'view';
        $tpl = new Template('com/disclaimer/template/disclaimer-'.str_replace('..','',strtolower($tpl)).'.tpl');
        $tpl->assign('formname','disclaimer');
        $tpl->assign('form',$form);
        $tpl->assign('url',URL);
        $tpl->assign('dataurl',DATAURL);
        $tpl->assign('isDialog',($request['op'] == 'loaddialog')?1:0);
        $tpl->assign('isAdmin',$service->get('User')->isAdmin());

        $ret = $tpl->render();
        $service->get('Ressource')->addScript('
            $(document).ready(function(){
                var isScrolledDown = false;
                $("textarea#disclaimer_fulltext").css("height","200px");
                $("textarea#disclaimer_fulltext").on("scroll", function() {
                    var offset = 200;
                    if (this.scrollHeight <= (this.scrollTop+offset)) {
                        isScrolledDown = true;
                    }
                });
                $("article.disclaimer [type=\'checkbox\']").click(function(){
                    $("[type=\'submit\']").prev(".msg.error").remove();
                    if (isScrolledDown) {
                        $("[type=\'submit\']").removeAttr("disabled");
                    }
                    else{
                        $("[type=\'submit\']").before("<p class=\"msg error\">'.DISCLAIMER_ERROR.'</p>");
                        setTimeout(function(){
                            $("article.disclaimer [type=\'checkbox\']").removeAttr("checked");
                        },150);
                    }

                });
            });
        ');
        return $ret;
    }
}
?>
