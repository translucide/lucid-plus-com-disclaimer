<?php
global $service;
$service->get('Ressource')->get('com/disclaimer/'.$service->get('Language')->getCode().'/setting.php');

/**
 * Collections of tasks to perform on install
 **/
$info = [
    'info' => [
        'title' => 'Disclaimer or TOS module you have to agree upon login',
        'description' => 'When a user logs in, he will need to agree on some terms and conditions.',
        'author' => 'Emmanuel Morin',
        'authorurl' => 'https://translucide.ca',
        'company' => 'Translucide',
        'companyurl' => 'https://translucide.ca',
        'url' => '',
        'version' => '0.1'
    ],
    'route' => [
        [
            'url' => '{lang}/disclaimer/{op}',
            'controller' => 'disclaimer'
        ],
    ],
    'setting' => [
        [
            'category' => 'disclaimer',
            'name' => 'fulltext',
            'value' => '',
            'type' => 'textarea',
            'options' => '',
            'title' => 'DISCLAIMER_SETTING_FULLTEXT',
            'description' => 'DISCLAIMER_SETTING_FULLTEXT_DESC',
        ],
        [
            'category' => 'disclaimer',
            'name' => 'mode',
            'value' => '',
            'type' => 'select',
            'options' => '1=Recurring', //,2=After Set Date
            'title' => 'DISCLAIMER_SETTING_MODE',
            'description' => 'DISCLAIMER_SETTING_MODE_DESC',
        ],
        [
            'category' => 'disclaimer',
            'name' => 'eachnbdays',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'DISCLAIMER_SETTING_EACHNBDAYS',
            'description' => 'DISCLAIMER_SETTING_EACHNBDAYS_DESC',
        ],
        /*[
            'category' => 'disclaimer',
            'name' => 'setdate',
            'value' => '',
            'type' => 'datepicker',
            'options' => '',
            'title' => 'DISCLAIMER_SETTING_SETDATE',
            'description' => 'DISCLAIMER_SETTING_SETDATE_DESC',
        ],*/
    ],
    'settingcategory' => [
        [
            'name' => 'disclaimer',
            'title' => 'DISCLAIMER_SETTING_CATEGORY'
        ]
    ]
];
