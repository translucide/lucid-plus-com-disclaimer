<article class="disclaimer view disclaimer-view">
    <h1>{:$smarty.const.DISCLAIMER_TITLE:}</h1>
    <section class="{:$formname:}">
        <div class="ct">
            <form class="form-vertical" id="{:$formname:}" name="{:$formname:}" method="POST" action="{:$url:}{:$lang:}/disclaimer/sign">
                <div id="{:$formname:}_div"><div class="warning"><noscript>Activate Javascript to view this form !</noscript></div></div>
                {:$form:}
            </form>
        </div>
    </section>
</article>
